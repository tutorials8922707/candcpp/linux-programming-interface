#include <time.h>
#include "curr_time.h"

#define BUFF_SIZE 1000

/* Return a string containing the current thime formatted axordin to
the specification if 'format' (see strftime(3) for specifiers).
If 'format' is NULL, we use "%c" as a specifier (which gives the
		date and time as for ctime(3), but without the trailing newline).
Return NULL on error. */
char * currTime(const char *format) {
	static char buf[BUFF_SIZE]; // Nonreentrant
	time_t t;
	size_t s;
	struct tm *tm;

	t = time(NULL);
	tm = localtime(&t);
	if (tm == NULL) {
		return NULL;
	}

	s = strftime(buf, BUFF_SIZE, (format != NULL) ? format : "%c", tm);

	return (s == 0) ? NULL : buf;
}
