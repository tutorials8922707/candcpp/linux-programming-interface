#include "error_functions.h"
#include "ugid_functions.h"

#include "tlpi_hdr.h"
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <libgen.h>
#include <pwd.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define FILENAME_SIZE 200
#define PROC_NAME_SIZE 200

static Boolean starts_with(const char *str, const char *sub_str) {
  int i;
  for (i = 0; i < strlen(sub_str); i++) {
    if (str[i] != sub_str[i]) {
      return FALSE;
    }
  }
  return TRUE;
}

static void build_proc_filename(char *file_name, const int count, ...) {
  int i;
  va_list ap;

  file_name[0] = '\0';
  va_start(ap, count);
  for (i = 0; i < count; i++) {
    strcat(file_name, va_arg(ap, char *));
  }
  va_end(ap);
}

static Boolean get_uid(const char *folder_name, unsigned int *uid) {
  char file_name[FILENAME_SIZE];
  FILE *fp;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
	int n;
  Boolean found;

  build_proc_filename(file_name, 3, "/proc/", folder_name, "/status");
  fp = fopen(file_name, "r");
  if (fp == NULL) {
		return FALSE;
  }
  while ((read = getline(&line, &len, fp)) != -1) {
    if (starts_with(line, "Uid:")) {
      n = sscanf(line, "Uid:\t%u", uid);
      if (n != EOF) {
        found = TRUE;
        break;
      }
    }
  }
  fclose(fp);
  if (line) {
    free(line);
  }
  return found;
}

static Boolean get_proc_name(const char *folder_name, char *result) {
  char file_name[FILENAME_SIZE];
  FILE *fp;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;

  build_proc_filename(file_name, 3, "/proc/", folder_name, "/cmdline");
  fp = fopen(file_name, "r");
  if (fp == NULL) {
		return FALSE;
  }
  if ((read = getline(&line, &len, fp)) != -1) {
		strcpy(result, basename(line));
	}
  fclose(fp);
  if (line) {
    free(line);
    line = NULL;
  }
	return TRUE;
}

int main(int argc, char *argv[]) {
  struct dirent *dp;
  DIR *dir;
  uid_t uid;

  unsigned int p_uid;
  Boolean found;
	char proc_name[PROC_NAME_SIZE];

  if (argc < 2) {
    errExit("username not specified");
  }

  uid = userIdFromName(argv[1]);

  dir = opendir("/proc");
  dp = readdir(dir);
  while ((dp = readdir(dir)) != NULL) {
    if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 &&
        strcmp(dp->d_name, "self") != 0 &&
        strcmp(dp->d_name, "thread-self") != 0) {
      found = get_uid(dp->d_name, &p_uid);
      if (found && p_uid == (unsigned int)uid) {
				if (get_proc_name(dp->d_name, proc_name)) {
					printf("%s\n", proc_name);
				}
      }
      found = FALSE;
    }
  }
  closedir(dir);

	exit(EXIT_SUCCESS);
}
