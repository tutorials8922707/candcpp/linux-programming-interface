#include "tlpi_hdr.h"
#include "ugid_functions.h"
#include <dirent.h>
#include <libgen.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>

#define FILENAME_SIZE 200
#define PROC_NAME_SIZE 200
#define MAX_PROC_COUNT 1000

struct pr {
  char name[PROC_NAME_SIZE];
  unsigned int ppid;
  unsigned int pid;
};

static Boolean starts_with(const char *str, const char *sub_str) {
  int i;
  for (i = 0; i < strlen(sub_str); i++) {
    if (str[i] != sub_str[i]) {
      return FALSE;
    }
  }
  return TRUE;
}

static void build_proc_filename(char *file_name, const int count, ...) {
  int i;
  va_list ap;

  file_name[0] = '\0';
  va_start(ap, count);
  for (i = 0; i < count; i++) {
    strcat(file_name, va_arg(ap, char *));
  }
  va_end(ap);
}

static Boolean get_process_info(const char *folder_name, unsigned int *ppid, unsigned int *pid, char * pname) {
  char file_name[FILENAME_SIZE];
  FILE *fp;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  int n;
  Boolean found = TRUE;

  build_proc_filename(file_name, 3, "/proc/", folder_name, "/status");
  fp = fopen(file_name, "r");
  if (fp == NULL) {
    return FALSE;
  }
  while ((read = getline(&line, &len, fp)) != -1) {
    if (starts_with(line, "Name:")) {
      n = sscanf(line, "Name:\t%s", pname);
      if (n == EOF) {
        found = FALSE;
        break;
      }
    } else if (starts_with(line, "PPid:")) {
      n = sscanf(line, "PPid:\t%u", ppid);
      if (n == EOF) {
        found = FALSE;
        break;
      }
    } else if (starts_with(line, "Pid:")) {
      n = sscanf(line, "Pid:\t%u", pid);
      if (n == EOF) {
        found = FALSE;
        break;
      }
    }
  }
  fclose(fp);
  if (line) {
    free(line);
  }
  return found;
}

static void printChilds(struct pr *processes, size_t processes_count, size_t parent_pos, unsigned int depth) {
	size_t i;
	unsigned int d;
	struct pr *parent = &processes[parent_pos];

	for (d = 0; d < depth; d++) {
		printf("\t");
		printf("|");
	}
	printf("%s %u %u\n", parent->name, parent->ppid, parent->pid);
	for (i = 0; i < processes_count; i++) {
		if (processes[i].ppid == parent->pid) {
			printChilds(processes, processes_count, i, depth + 1);
		}
	}
}

int main(int argc, char *argv[]) {
  struct dirent *dp;
  DIR *dir;

  unsigned int ppid;
  unsigned int pid;
  Boolean found;
  char proc_name[PROC_NAME_SIZE];
  struct pr processes[MAX_PROC_COUNT];
  size_t processes_count = 0;

  unsigned int k = 0;
  unsigned int j;
  size_t i;
  Boolean is_all_printed;
	size_t init_pos;
	Boolean init_found = FALSE;

  dir = opendir("/proc");
  dp = readdir(dir);
  while ((dp = readdir(dir)) != NULL) {
    if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 &&
        strcmp(dp->d_name, "self") != 0 &&
        strcmp(dp->d_name, "thread-self") != 0) {
      if (get_process_info(dp->d_name, &ppid, &pid, proc_name)) {
        strcpy(processes[processes_count].name, proc_name);
        processes[processes_count].ppid = ppid;
        processes[processes_count].pid = pid;
				if (!init_found) {
					if (strcmp(processes[processes_count].name, "systemd") == 0) {
						init_found = TRUE;
						init_pos = processes_count;
					}
				}
        processes_count += 1;
      }
      found = FALSE;
    }
  }
  closedir(dir);

	if (init_found) {
		printChilds(processes, processes_count, init_pos, 0);
	}

  exit(EXIT_SUCCESS);
}
