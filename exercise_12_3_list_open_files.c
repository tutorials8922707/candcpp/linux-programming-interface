#include "tlpi_hdr.h"
#include <dirent.h>
#include <linux/limits.h>
#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdarg.h>

#define FILENAME_SIZE 200
#define PROC_NAME_SIZE 200

static Boolean starts_with(const char *str, const char *sub_str) {
  int i;
  for (i = 0; i < strlen(sub_str); i++) {
    if (str[i] != sub_str[i]) {
      return FALSE;
    }
  }
  return TRUE;
}

static void build_name(char *file_name, const int count, ...) {
  int i;
  va_list ap;

  file_name[0] = '\0';
  va_start(ap, count);
  for (i = 0; i < count; i++) {
    strcat(file_name, va_arg(ap, char *));
  }
  va_end(ap);
}

static Boolean get_process_name(const char *folder_name, char *proc_name) {
  char file_name[FILENAME_SIZE];
  FILE *fp;
  char *line = NULL;
  size_t len = 0;
  ssize_t read;
  int n;
  Boolean found = TRUE;

  build_name(file_name, 3, "/proc/", folder_name, "/status");
  fp = fopen(file_name, "r");
  if (fp == NULL) {
    return FALSE;
  }
  while ((read = getline(&line, &len, fp)) != -1) {
    if (starts_with(line, "Name:")) {
      n = sscanf(line, "Name:\t%s", proc_name);
      if (n == EOF) {
        found = FALSE;
        break;
      }
    } 
  }
  fclose(fp);
  if (line) {
    free(line);
  }
  return found;
}

int main(int argc, char *argv[])
{
  struct dirent *pr_dp;
  DIR *pr_dir;

  struct dirent *fd_dp;
  DIR *fd_dir;

	char proc_name[PROC_NAME_SIZE];
	char fd_dir_path[PATH_MAX];
	char fd_path[PATH_MAX];
	char real_path[PATH_MAX];

  pr_dir = opendir("/proc");
  pr_dp = readdir(pr_dir);

  while ((pr_dp = readdir(pr_dir)) != NULL) {
    if (strcmp(pr_dp->d_name, ".") != 0 && strcmp(pr_dp->d_name, "..") != 0 &&
        strcmp(pr_dp->d_name, "self") != 0 &&
        strcmp(pr_dp->d_name, "thread-self") != 0) {
			if (get_process_name(pr_dp->d_name, proc_name)) {
				printf("%s\n", proc_name);

				build_name(fd_dir_path, 3, "/proc/", pr_dp->d_name, "/fd");
				fd_dir = opendir(fd_dir_path);
				if (fd_dir == NULL) {
					printf("Can not read directory: %s\n\n", fd_dir_path);
					continue;
				}
				fd_dp = readdir(pr_dir);

				while ((fd_dp = readdir(fd_dir)) != NULL) {
					build_name(fd_path, 4, "/proc/", pr_dp->d_name, "/fd/", fd_dp->d_name);
					if(readlink(fd_path, real_path, PATH_MAX) == -1) {
						printf("failed to readlink() %s\n", fd_path);
					} else {
						printf("\t%s\n", real_path);
					}
				}
				printf("\n");

				closedir(fd_dir);
			}
		}
	}

	closedir(pr_dir);

	exit(EXIT_SUCCESS);
}
