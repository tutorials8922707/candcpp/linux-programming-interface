#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/xattr.h>
#include <unistd.h>

// ./a.out -n name -v value <file>
#include "error_functions.h"
int main(int argc, char *argv[])
{
	int opt;
	char *name, *value, *filename;

	if (argc < 2) {
		usageErr("%s -n <name> -v <value>\n", argv[0]);
	}
	
	while ((opt = getopt(argc, argv, "n:v:")) != -1) {
		switch (opt) {
			case 'n': name = optarg; break;
			case 'v': value = optarg; break;
			case '?':
				if (optopt == 'n' || optopt == 'v') {
					usageErr("-%c requires an argument\n", optopt);
				} else {
					usageErr(argv[0]);
				}
				break;
		}
	}

	filename = argv[optind];

	if (setxattr(filename, name, value, strlen(value), 0) == -1 ) {
		errExit("setxattr");
	}

	exit(EXIT_SUCCESS);
}
