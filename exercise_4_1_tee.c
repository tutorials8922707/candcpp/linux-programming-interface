
// tee file
// tee -a file

#include "error_functions.h"
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
#define BUFF_SIZE 50

  int writefd;
  char buf[BUFF_SIZE];
  int num;
  char *filename;

  int writePerms = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH;
  int writeFlags = O_CREAT | O_WRONLY;

  if (argc < 2) {
    usageErr("%s {<file>|-a <file>}\n", argv[0]);
  } else {
    int opt;

    while ((opt = getopt(argc, argv, ":a")) != -1) {
      switch (opt) {
      case 'a':
        writeFlags |= O_APPEND;
        // filename = optarg;
        break;
      case ':':
        usageErr("Missing argument\n");
        break;
      case '?':
        usageErr("Unrecognized option\n");
        break;
      }
    }
  }

  filename = argv[optind];

  writefd = open(filename, writeFlags, writePerms);

  while ((num = read(STDIN_FILENO, buf, BUFF_SIZE))) {
    if (write(writefd, buf, num) != num) {
      fatal("Could't write whole buffer\n");
    }
    printf("%s", buf);
  }

  close(writefd);
}
