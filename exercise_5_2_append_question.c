#include "error_functions.h"
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
#define BUF_SIZE 50
	int fd;
	char buf[BUF_SIZE] = "this text added\n";

	if (argc < 2 && strcmp(argv[1], "--help") == -1) {
		usageErr("%s <filename>", argv[0]);
	}

	fd = open(argv[1], O_WRONLY | O_APPEND);
	if (fd == -1) {
		errExit("open %s", argv[1]);
	}

	lseek(fd, 0, SEEK_SET);

	if (write(fd, &buf, BUF_SIZE) == -1) {
		errExit("write %s", argv[1]);
	}

	close(fd);
	exit(EXIT_SUCCESS);
}

// text is added at the end of file
