// Usage ./a.out f1 1000000 & ./a.out f1 x to use lseek(fd, 0, SEEK_END);
// Usage ./a.out f1 1000000 x & ./a.out f1 1000000 to use O_APPEND

/*
	Explanation
	without x appending secuentaly causing currouption
	with x using atomic append (corruption didn't happended)
*/

#include <stdlib.h>
#include "error_functions.h"
#include "get_num.h"
#include "tlpi_hdr.h"
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
	char *filename;
	long bytes;
	int flags = O_CREAT | O_WRONLY | O_APPEND;
	Boolean append = TRUE;

	int fd;

	if (argc < 3) {
		usageErr("%s <filename> <num-bytes> [x]\n", argv[0]);
	}

	filename = argv[1];
	bytes = getLong(argv[2], GN_GT_O , "num-bytes");
	if (argc < 4 || strcmp(argv[3], "x") == -1) {
		append = FALSE;
	}

	if (!append) {
		printf("asdf");
		flags ^= O_APPEND;
	}

	fd = open(filename, flags, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
	if(fd == -1) {
		errExit("open");
	}

	if (!append) {
		lseek(fd, 0, SEEK_END);
	}

	char w = 'w';

	long i;
	for (i = 0; i < bytes; i++) {
		if (write(fd, &w, sizeof(char)) == -1) {
			errExit("write");
		}
	}

	close(fd);
	exit(EXIT_SUCCESS);
}

