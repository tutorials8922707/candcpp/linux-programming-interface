#include <asm-generic/errno-base.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "error_functions.h"


int my_dup(int fd) {
	// check for validity
	if (fcntl(fd, F_GETFL) == -1) {
		errno = EBADF;
		return -1;
	}

	// duplicate fd
	int result = fcntl(fd, F_DUPFD, 0);
	if(result == -1) {
		return -1;
	}
	// TODO thing about this
	// close old fd
	// close(fd);

	return result;
}

int my_dup2(int oldFd, int newFd) {
	// check for validity
	if (oldFd == newFd && fcntl(oldFd, F_GETFL) == -1) {
		errno = EBADF;
		return -1;
	}
	// duplicate fd
	int result = fcntl(oldFd, F_DUPFD, newFd);
	if(result == -1) {
		return -1;
	}
	// TODO thing about this
	// close old fd
	// close(oldFd);
	return result;
}

static void write_to_output() {
	int res = my_dup(STDERR_FILENO);
	if (res == -1) {
		errExit("my_dup");
	}

	int num = write(res, "1\n", 2 * sizeof(char));
	if (num == -1) {
		errExit("write");
	}
}

static void write_to_output2() {
	int res = my_dup2(STDERR_FILENO, 10);
	if (res == -1) {
		errExit("my_dup2");
	}

	int num = write(res, "a\n", 2 * sizeof(char));
	if (num == -1) {
		errExit("write");
	}
}

static void write_to_output3() {
	int fd = open("name", O_CREAT | O_WRONLY | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
	if (fd == -1) {
		errExit("open");
	}

	fd = my_dup2(fd, 11);

	char buf[50];
	snprintf(buf, 50, "%d\n", fd);
	if(write(fd, buf, 50) == -1) {
		errExit("write");
	}

	close(fd);
}

int main(int argc, char *argv[])
{
	write_to_output3();

	exit(EXIT_SUCCESS);
}
