#include "error_functions.h"
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
int main(int argc, char *argv[])
{
	int flags = O_WRONLY;
	int perms = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP;

	int fd = open("name", flags, perms);
	if (fd == -1) {
		errExit("open fd1");
	}

	lseek(fd, 10, SEEK_SET);

	int fd_flags = fcntl(fd, F_GETFL);
	if (fd_flags == -1) {
		errExit("fcntl F_GETFL");
	}
	printf("fd: flags %d\n", fd_flags);

	fd = dup2(fd, 10);
	if (fd == -1) {
		errExit("optn fd2");
	}

	fd_flags = fcntl(fd, F_GETFL);
	if (fd_flags == -1) {
		errExit("fcntl F_GETFL");
	}
	printf("fd: flags %d\n", fd_flags);

	char buf[50] = "***this is it***";
	if(write(fd, buf, strlen(buf)) == -1) {
		errExit("write");
	}

	close(fd);

	exit(EXIT_SUCCESS);
}
