#include <setjmp.h>
#include <stdio.h>
#include <stdlib.h>

/*
	 NOTE I Think this is not what he menat
*/

static jmp_buf env;

static void some()
{
	if(setjmp(env) == 0) {
		printf("0");
	} else {
		printf("Jump");
	}
}

int main(int argc, char *argv[])
{
	some();

	longjmp(env, 1);

	exit(EXIT_SUCCESS);
}
