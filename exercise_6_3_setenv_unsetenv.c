#include "tlpi_hdr.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

extern char **environ;

static Boolean starts_with(const char* str, const char* sub_str) {
	int i;
	for (i = 0; i < strlen(sub_str); i++) {
		if (str[i] != sub_str[i]) {
			return FALSE;
		}
	}
	return TRUE;
}

int my_setenv(const char *name, const char *value, int replace) {
	// char buf[100];
	// snprintf(buf, 100, "%s=%s", name, value);

	char *buf = (char *) malloc(strlen(name) + strlen(value));
	strcpy(buf, name);
	strcat(buf, "=");
	strcat(buf, value);

	if (getenv(name) != NULL) {
		if (replace == 1) {
			return putenv(buf);
		}
	} else {
		return putenv(buf);
	}

	return 1;
}


int my_unsetenv(const char *name) {
	// FIXME corrent the deletin algorithm
	unsigned int removed = 0;
	char **ep = environ;
	while (*ep != NULL) {
		if (starts_with(*ep, name)) {
			removed++;
			free(*ep);
		}
		*ep = *(ep + removed);
		ep++;
	}
	return removed;
}

int main(int argc, char *argv[])
{
	my_setenv("Alaska", "val", 1);
	my_setenv("Alaska", "some", 0);

	// my_unsetenv("Alaska");

	char **ep;

	for (ep = environ; *ep != NULL; ep++) {
		puts(*ep);
	}

	exit(EXIT_SUCCESS);
}
