#include <pwd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "error_functions.h"



struct passwd g_passwd;

struct passwd * my_getpwnam(const char * username)
{
	struct passwd *pwd;
	while ((pwd = getpwent()) != NULL) {
		if (strcmp(username, pwd->pw_name) == 0) {
			g_passwd.pw_name = pwd->pw_name;
			g_passwd.pw_passwd = pwd->pw_passwd;
			g_passwd.pw_uid = pwd->pw_uid;
			g_passwd.pw_dir = pwd->pw_dir;
			g_passwd.pw_gid = pwd->pw_gid;
			g_passwd.pw_gecos = pwd->pw_gecos ;
			g_passwd.pw_shell = pwd->pw_shell;
			endpwent();
			return &g_passwd;
		}
	}
	endpwent();
	return NULL;
}

int main(int argc, char *argv[])
{
	struct passwd *pwd;

	pwd = my_getpwnam("ach");
	if (pwd == NULL) {
		errExit("not found");
	}

	printf("%s\n", pwd->pw_dir);

	exit(EXIT_SUCCESS);
}
