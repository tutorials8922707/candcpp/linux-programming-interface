#include "error_functions.h"
#include <stdio.h>
#include <stdlib.h>
#define _BNU_SOURCE
#include <unistd.h>
#include <sys/fsuid.h>
#include <limits.h>
#include "ugid_functions.h"
#include "tlpi_hdr.h"

#define SG_SIZE (NGROUPS_MAX + 1)

int main(int argc, char *argv[])
{
	uid_t ruid, euid, suid, fsuid;
	gid_t rgid, egid, sgid, fsgid;
	gid_t suppGroups[SG_SIZE];
	int numGroups, j;
	char *p;

	if (getresuid(&ruid, &euid, &suid) == -1) {
		errExit("getresuid");
	}
	if (getresgid(&rgid, &egid, &sgid) == -1) {
		errExit("getresgid");
	}

	/* Attempts to change the file-system IDs are always ignored
	for unpriviliged processes, but even so, the follwing
	calls return the current file-system IDs */

	fsuid = setfsuid(0);
	fsgid = setfsgid(0);


	printf("UID: ");
	p = userNameFromId(ruid);
	printf("real=%s (%ld); ", (p == NULL) ? "???" : p, (long) ruid);

	p = userNameFromId(euid);
	printf("eff=%s (%ld); ", (p == NULL) ? "???" : p, (long) euid);

	p = userNameFromId(suid);
	printf("saved=%s (%ld); ", (p == NULL) ? "???" : p, (long) suid);

	p = userNameFromId(fsuid);
	printf("fs=%s (%ld); ", (p == NULL) ? "???" : p, (long) fsuid);
	printf("\n");

	printf("GID: ");
	p = userNameFromId(ruid);
	printf("real=%s (%ld); ", (p == NULL) ? "???" : p, (long) rgid);

	p = userNameFromId(euid);
	printf("eff=%s (%ld); ", (p == NULL) ? "???" : p, (long) egid);

	p = userNameFromId(suid);
	printf("saved=%s (%ld); ", (p == NULL) ? "???" : p, (long) sgid);

	p = userNameFromId(fsuid);
	printf("fs=%s (%ld); ", (p == NULL) ? "???" : p, (long) fsgid);
	printf("\n");

	numGroups = getgroups(SG_SIZE, suppGroups);
	if (numGroups == -1) {
		errExit("getgroups");
	}

	printf("Supplementary groups (%d): ", numGroups);
	for (j = 0; j < numGroups; j++) {
		p = groupNameFromId(suppGroups[j]);
		printf("%s (%ld) ", (p == NULL) ? "???" : p, (long) suppGroups[j]);
	}

	printf("\n");
	
	exit(EXIT_SUCCESS);
}

