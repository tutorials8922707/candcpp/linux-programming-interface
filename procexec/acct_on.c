#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define _BSD_SOURCE
#include "../tlpi_hdr.h"
#include <unistd.h>

int main(int argc, char *argv[]) {
  if (argc > 2 || (argc > 1 && strcmp(argv[1], "--help") == 0)) {
    usageErr("%s [file]\n");
  }

  if (acct(argv[1]) == -1) {
    errExit("acct");
  }
  printf("Process accounting %s \n",
         (argv[1] == NULL) ? "disabled" : "enabled");
  exit(EXIT_SUCCESS);
}
