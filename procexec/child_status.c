#include "../tlpi_hdr.h"
#include "print_wait_status.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
  int status;
  pid_t childPid;

  if (argc > 1 && strcmp(argv[1], "--help") == 0) {
    usageErr("%s [exit-status]\n", argv[0]);
  }

  switch (fork()) {
  case -1:
    errExit("fork");
  case 0:
    printf("Child started with PID = %ld\n", (long)getpid());
    if (argc > 1) { // Status supplied on command line?
      exit(getInt(argv[1], 0, "exit-status"));
    } else { // Otherwise, wiat for signals
      for (;;) {
        pause();
      }
      exit(EXIT_FAILURE); // Not reached, but good practice
    }
  default:
    for (;;) {
      childPid = waitpid(-1, &status,
                         WUNTRACED
#ifdef WCONTINUED
                             | WCONTINUED
#endif
      );
      if (childPid == -1) {
        errExit("waitpid");

        // Print status in hex, and as separate decimal bytes

        printf("waitpid() returned: PID=%ld, status=0x%04x (%d,%d)\n",
               (long)childPid, (unsigned int)status, status >> 8,
               status & 0xff);
        printWaitStatus(NULL, status);

        if (WIFEXITED(status) || WIFSIGNALED(status)) {
          exit(EXIT_SUCCESS);
        }
      }
    }
  }
}
