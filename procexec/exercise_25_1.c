#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "../tlpi_hdr.h"

int main(int argc, char *argv[])
{
	switch (fork()) {
		case -1:
			errExit("fork");
		case 0:
			sleep(1);
			exit(-1);
		default:
			printf("%d", WEXITSTATUS(wait(NULL)));
	}

	exit(EXIT_SUCCESS);
}
