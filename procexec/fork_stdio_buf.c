#include "../tlpi_hdr.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
	printf("Hello world\n");
	write(STDOUT_FILENO, "Ciao\n", 5);

	if (fork()) {
		errExit("fork");
	}

	// Both child and parent continue executio here

	exit(EXIT_SUCCESS);
}

