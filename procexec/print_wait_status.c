#define _GNU_SOURCE
#include "print_wait_status.h"
#include "../tlpi_hdr.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>

/* NOTE: The following function emplys printf(), which is not
         async-signal-safe (see Section 21.1.2). As such, this function is
         also no async-signal-safe (i.e, beware of calling it from a
         SIGCHLD handler). */

// Examine a wait() status using the W* macros
void printWaitStatus(const char *msg, int status) {
  if (msg != NULL) {
    printf("%s", msg);
  }

  if (WIFEXITED(status)) {
    printf("child exited, status=%d\n", WEXITSTATUS(status));
  } else if (WIFSIGNALED(status)) {
    printf("child killed by signal %d (%s)", WTERMSIG(status),
           strsignal(WTERMSIG(status)));
#ifdef WCOREDUMP
    if (WCOREDUMP(status)) {
      printf(" (core dumped)");
    }
#endif

  } else if (WIFSTOPPED(status)) {
    printf("child stopped by signal %d (%s)\n", WSTOPSIG(status),
           strsignal(WSTOPSIG(status)));
    // SUSv3 has this, but older Linux version and
    // some other UNIX impementations don't
#ifdef WIFCONTINUED
  } else if (WIFCONTINUED(status)) {
    printf("child continues\n");
#endif
  } else {
    printf("what happend to this child? (status=%x)\n", (unsigned int)status);
  }
}
