#define _GNU_SOURCE

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include "../tlpi_hdr.h"
#include <fcntl.h>
#include <sched.h>
#include <signal.h>
#include <stdlib.h>
#include <sys/wait.h>
#include <unistd.h>

#ifndef CHILD_SIG
// Signal to be generated on termination of cloned child
#define CHILD_SIG SIGUSR1
#endif

// Startup function for cloned child
static int childFunc(void *arg) {
  if (close(*((int *)arg)) == -1) {
    errExit("close");
  }

  // Child terminates now
  return 0;
}

int main(int argc, char *argv[]) {
  // Stack size of cloned child
  const int STACK_SIZE = 65536;
  // Start of stack buffer
  char *stack;
  // End of stack buffer
  char *stackTop;

  int s, fd, flags;

  // Child will close this fd
  fd = open("/dev/null", O_RDWR);
  if (fd == -1) {
    errExit("open");
  }

  // if argc > 1, child shares file descriptor table with parent

  flags = (argc > 1) ? CLONE_FILES : 0;

  // Alocate stack for child

  stack = malloc(STACK_SIZE);
  if (stack == NULL) {
    errExit("malloc");
  }
  // Assume stack grows downward
  stackTop = stack + STACK_SIZE;

  // Ignore CHILD_SIG, in case it is a signal whose default is to
  // terminate the process; but don't ignore SIGCHLD (which is ignored
  // by default), since that would prevent the creation of a zombie
  if (CHILD_SIG != 0 && CHILD_SIG != SIGCHLD) {
    if (signal(CHILD_SIG, SIG_IGN) == SIG_ERR) {
      errExit("signal");
    }
  }

  // Create child; child commences execution in childFunc()
  if (clone(childFunc, stackTop, flags | CHILD_SIG, (void *)&fd) == -1) {
    errExit("clone");
  }

  // Parent falls through to here. Wait for child; __WCLONE is
  // needed for child notifying with signal other than SIGCHLD

  if (waitpid(-1, NULL, (CHILD_SIG != SIGCHLD) ? __WCLONE : 0) == -1) {
    errExit("waitpid");
  }
  printf("child has terminated\n");

  // Did close() of file descriptor in child affect parent?
  s = write(fd, "x", 1);
  if (s == -1 & errno == EBADF) {
    printf("file descriptor %d has been closed\n", fd);
  } else if (s == -1) {
    printf("write() on file descriptor %d failed "
           "unexpectedly (%s)\n",
           fd, strerror(errno));
  } else {
    printf("write() on file descriptor %d succeeded\n", fd);
  }

  exit(EXIT_SUCCESS);
}
