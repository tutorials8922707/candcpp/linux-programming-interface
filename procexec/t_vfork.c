#include "../tlpi_hdr.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
	int istack = 22;

	switch (vfork()) {
		case -1:
			errExit("vfork");
		case 0: // Child executes first, in parent's memory space
			sleep(3); // Even if we sleep for a while parent still is no scheduled

			write(STDOUT_FILENO, "Child executing\n", 16);
			istack *= 3; // This change will be seen by parent
			_exit(EXIT_SUCCESS);
		default:
			write(STDOUT_FILENO, "Parent executing\n", 17);
			printf("Istack=%d\n", istack);
			exit(EXIT_SUCCESS);
	}
}


