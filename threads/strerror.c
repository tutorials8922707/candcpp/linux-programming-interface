#define _GNU_SOURCE
// Get '_sys_nerr' and '_sys_errlist' desclarations from <stdio.h>

// Get declarations of strerror()
#include <stdio.h>
#include <string.h>

// Maximum length of string returned by strerror()
#define MAX_ERROR_LEN 256

// Statically allocated return buffer
static char buf[MAX_ERROR_LEN];

char * strerror(int err) {
	if (err < 0 || err >= _sys_nerr || _sys_errlist[err] == NULL) {
		snprintf(buf, MAX_ERROR_LEN, "Unknown error %d", err);
	} else {
		strncpy(buf, _sys_errlist[err], MAX_ERROR_LEN - 1);
		buf[MAX_ERROR_LEN - 1] = '\0';
	}

	return buf;
}
