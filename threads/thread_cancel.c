#include "../tlpi_hdr.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

static void *threadFunc(void *arg) {
  int j;
  printf("New thread started\n");
  for (j = 1;; j++) {
    printf("Loop %d\n", j);
    sleep(1);
  }

  // NOTREACHED
  return NULL;
}

int main(int argc, char *argv[]) {
  pthread_t thr;
  int s;
  void *res;

  s = pthread_create(&thr, NULL, threadFunc, NULL);
  if (s != 0) {
    errExitEn(s, "pthread_create");
  }

  sleep(3);

  s = pthread_cancel(thr);
  if (s != 0) {
    errExitEn(s, "pthread_join");
  }

	s = pthread_join(thr, &res);
	if (s != 0) {
		errExitEn(s, "pthread_join");
	}

  if (res == PTHREAD_CANCELED) {
    printf("Thread was canceled");
  } else {
    printf("Thread was no canceled (should no happen!)\n");
  }

  exit(EXIT_SUCCESS);
}
