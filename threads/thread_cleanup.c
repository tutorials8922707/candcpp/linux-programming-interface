#include "../tlpi_hdr.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

static pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t mtx = PTHREAD_MUTEX_INITIALIZER;
static int glob = 0;

// Free memory pointed to by 'arg' and unlock mutex
static void cleanupHandler(void *arg) {
  int s;

  printf("cleanup: freeing block at %p\n", arg);
  free(arg);

  printf("cleanup: unlocking mutex\n");
  s = pthread_mutex_unlock(&mtx);
  if (s != 0) {
    errExitEn(s, "pthread_mutex_unlock");
  }
}

static void *threadFunc(void *arg) {
  int s;
  // Buffer allocated by thread
  void *buf = NULL;

  // Not a cancellation point
  buf = malloc(0x100000);
  printf("thread: allocated memory at %p\n", buf);

  s = pthread_mutex_lock(&mtx);
  if (s != 0) {
    errExitEn(s, "pthread_mutex_lock");
  }

  pthread_cleanup_push(cleanupHandler, buf);

  while (glob == 0) {
    // A cancellation point
    s = pthread_cond_wait(&cond, &mtx);
    if (s != 0) {
      errExitEn(s, "pthread_cond_wait");
    }
  }

  printf("thread: condition wait loop completed\n");
	// Executes cleanup handler
  pthread_cleanup_pop(1);
  return NULL;
}

int main(int argc, char *argv[])
{
	pthread_t thr;
	void *res;
	int s;

	s = pthread_create(&thr, NULL, threadFunc, NULL);
	if (s != 0) {
		errExitEn(s, "pthread_create");
	}

	// Give thread a chance to get started
	sleep(2);

	if (argc == 1) {
		printf("main: about to cancel thread\n");
		s = pthread_cancel(thr);
		if (s != 0) {
			errExitEn(s, "pthread_cancel");
		}
	} else {
		// signal condition variable
		printf("main: about to signal condition variable\n");
		glob = 1;
		s = pthread_cond_signal(&cond);
		if (s != 0) {
			errExitEn(s, "pthread_cond_signal");
		}
	}

	s = pthread_join(thr, &res);
	if (s != 0) {
		errExitEn(s, "pthread_join");
	}
	if (res == PTHREAD_CANCELED) {
		printf("main: thread was canceled\n");
	} else {
		printf("main: thread terminated normally\n");
	}

	exit(EXIT_SUCCESS);
}
