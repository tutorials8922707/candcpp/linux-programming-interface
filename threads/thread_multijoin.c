#include "../tlpi_hdr.h"
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static pthread_cond_t threadDied = PTHREAD_COND_INITIALIZER;
static pthread_mutex_t threadMutex = PTHREAD_MUTEX_INITIALIZER;

// Portects all of the following global variables

// Total number of threads created
static int totThreads = 0;
// Total number of threads still alive of terminated but not yet joined
static int numLive = 0;
// Number of terminated threads that have not yet been joined
static int numUnjoined = 0;

// Thread states
enum tstate {
  // Thread is alive
  TS_ALIVE,
  // Thread is terminated, not yet joined
  TS_TERMINATED,
  // thread terminated, and joined
  TS_JOINED
};

// Info about each thread
static struct {
  // ID of this thread
  pthread_t tid;
  // Thread state
  enum tstate state;
  // Number seconds to live before terminating
  int sleepTime;
} *thread;

static void *threadFunc(void *arg) {
  int idx = *((int *)arg);
  int s;

  // Simulate doing some work
  sleep(thread[idx].sleepTime);
  printf("Thread %d terminating\n", idx);

  s = pthread_mutex_lock(&threadMutex);
  if (s != 0) {
    errExitEn(s, "pthread_mutex_lock");
  }

  numUnjoined++;
  thread[idx].state = TS_TERMINATED;

  s = pthread_mutex_unlock(&threadMutex);
  if (s != 0) {
    errExitEn(s, "pthread_mutex_unlock");
  }
  s = pthread_cond_signal(&threadDied);
  if (s != 0) {
    errExitEn(s, "pthread_cond_signal");
  }

  return NULL;
}

int main(int argc, char *argv[]) {
  int s, idx;

  if (argc < 2 || strcmp(argv[1], "--help") == 0) {
    usageErr("%s nsecs...\n", argv[0]);
  }

  thread = calloc(argc - 1, sizeof(*thread));
  if (thread == NULL) {
    errExit("calloc");
  }

  // Create all threads

  for (idx = 0; idx < argc; idx++) {
    thread[idx].sleepTime = getInt(argv[idx + 1], GN_NONNEG, NULL);
    thread[idx].state = TS_ALIVE;
    s = pthread_create(&thread[idx].tid, NULL, threadFunc, &idx);
    if (s != 0) {
      errExitEn(s, "pthread_create");
    }
  }

  totThreads = argc - 1;
  numLive = totThreads;

  // Join with terminated threads
  while (numLive > 0) {
    s = pthread_mutex_lock(&threadMutex);
    if (s != 0) {
      errExitEn(s, "pthread_mutex_lock");
    }

    while (numUnjoined == 0) {
      s = pthread_cond_wait(&threadDied, &threadMutex);
      if (s != 0) {
        errExitEn(s, "pthread_cond_wait");
      }
    }

    for (idx = 0; idx < totThreads; idx++) {
      if (thread[idx].state == TS_TERMINATED) {
        s = pthread_join(thread[idx].tid, NULL);
        if (s != 0) {
          errExitEn(s, "pthread_join");
        }

        thread[idx].state = TS_JOINED;
        numLive--;
        numUnjoined--;

        printf("Reaped thread %d (numLive=%d)\n", idx, numLive);
      }
    }

    s = pthread_mutex_unlock(&threadMutex);
    if (s != 0) {
      errExitEn(s, "pthread_mutex_unlock");
    }
  }
  exit(EXIT_SUCCESS);
}
