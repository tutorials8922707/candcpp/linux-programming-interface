#ifndef TLPI_HDR_H
#define TLPI_HDR_H /* Prevent accidental double inclusion */

#include <stdio.h> /* Type definitions used by many programs */
#include <stdlib.h> /* Standard I/O functions */
#include <sys/types.h> /* Prototypes of commonly used library functions, plus EXIT_SUCCESS and EXIT_FAILURE constants */

#include <errno.h> /* Declares errno and defines error constants */
#include <string.h> /* Commonly used string-handling function */
#include <unistd.h> /* Prototypes for many system calls */

#include "get_num.h" /* Declares our functions for handling numeric arguments (getInt(), getLing()) */

#include "error_functions.h" /* Declares our error-handling function */

typedef enum { FALSE, TRUE } Boolean;

#define min(m, n) ((m) < (n) ? (m) : (n))
#define max(m, n) ((m) > (n) ? (m) : (n))

#endif // !TLPI_HDR_H
